package db;

import scheme.MPIPatient;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    private Database database;
    public Repository(Database db) {this.database = db;}

    public List<MPIPatient> getChunk(long startIndex, long limit) {

        List<Object> params = new ArrayList<>();

        params.add(startIndex);
        params.add(limit);

        String query = "";

        query = "select p.id pid,\n" +
                    "       p.last_name,\n" +
                    "       p.middle_name,\n" +
                    "       p.first_name,\n" +
                    "       p.gender,\n" +
                    "       p.birth_date,\n" +
                    "       p.snils,\n" +
                    "       p.enp,\n" +
                    "       p.dead,\n" +
                    "       p.death_date,\n" +
                    "       a.id,\n" +
                    "       a.date_in,\n" +
                    "       a.date_out,\n" +
                    "       a.type,\n" +
                    "        o.ogrn,\n" +
                    "       a.district,\n" +
                    "       a.owner_snils\n" +
                    "from core.person_characteristics pc\n" +
                    "inner join core.person p on p.id = pc.person\n" +
                    "inner join core.attachment a on a.id = pc.attachment\n" +
                    "inner join core.organization o on a.mo = o.id\n" +
                    "where p.id > ?\n" +
                    "order by p.id\n" +
                    "limit ?;";

        return database.executeSelect(query, params, Mapper::mpiPatients);
    }

    public Long getMaxId() {
        return  database.executeSelect(
                "select max(person) max from core.person_characteristics",
                null, (Mapper::applyMax));
    }


}
