package db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vibur.dbcp.ViburDBCPDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Database {
    private static final Logger LOG = LoggerFactory.getLogger(Database.class);

    private ViburDBCPDataSource ds;
    //private Connection connection;
    List<PreparedStatement> statements;


    public Database() {
        ds = initDS();
    }

    private ViburDBCPDataSource initDS() {

        ViburDBCPDataSource ds = new ViburDBCPDataSource();
        statements = new ArrayList<>();


        ds.setJdbcUrl("jdbc:postgresql://q4-demo2.medsoft.su:6432/mpi?prepareThreshold=0&characterEncoding=utf8");
        ds.setUsername("mpi");
        ds.setPassword("mpi");
        ds.setLogConnectionLongerThanMs(-1);
        ds.setLogLargeResultSet(-1);
        ds.start();
        return ds;
    }

    public void terminate() {
        ds.terminate();
    }

    private void prepareStatement(PreparedStatement ps, List<Object> params) throws SQLException {
        if(params == null)
            return;

        for (int i = 0; i < params.size(); i++) {
            ps.setObject(i + 1, params.get(i));
        }
    }


    public <T> T prepareSelect(Connection c, String sql, List<Object> params, Function<ResultSet, T> mapper) {
        try(PreparedStatement ps = c.prepareStatement(sql)) {
            prepareStatement(ps, params);
            return fetchData(ps, mapper);
        } catch (SQLException e) {
            LOG.error("ошибка при формировании");
            throw new RuntimeException("ошибка при формировании", e);
        }
    }
    public <T> T executeSelect(String sql, List<Object> params, Function<ResultSet, T> mapper) {
        try (Connection c = ds.getConnection()) {
            return prepareSelect(c, sql, params, mapper);
        } catch (SQLException e) {
            LOG.error("Ошибка при выполнении запроса к бд", e);
            throw new RuntimeException(e);
        }
    }

    private <T> T fetchData(PreparedStatement ps, Function<ResultSet, T> mapper) {
        try(ResultSet rs = ps.executeQuery()) {
            return mapper.apply(rs);
        } catch (SQLException e) {
            LOG.error("ошибка при получении данных из db");
            throw new RuntimeException("ошибка при получении данных из db", e);
        }
    }

}
