package db;

import core.CONSTANTS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scheme.MPIAttachment;
import scheme.MPIPatient;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;

public class Mapper {
    private static final Logger LOG = LoggerFactory.getLogger(Mapper.class);
    private static final DateTimeFormatter ldtFormatter = new DateTimeFormatterBuilder()
            .appendPattern(core.CONSTANTS.DATE_FORMAT)
            .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
            .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
            .toFormatter();

    static Long applyMax(ResultSet rs) {
        Long id = -1L;
        try {
            if (rs.next()) id = rs.getLong("max");
        } catch (SQLException e) {
            LOG.error(CONSTANTS.MAPPING_ID_ERROR, e);
        }
        return id;
    }

    public static List<MPIPatient> mpiPatients(ResultSet rs) {
        List<MPIPatient> patients = new ArrayList<>();

        try {
            while(rs.next()) {
                MPIPatient patient = new MPIPatient();

                patient.setId(rs.getLong("pid"));
                patient.setBirthDate(ldtFormatter.format(rs.getDate("birth_date").toLocalDate()));
                patient.setEnp(rs.getString("enp"));
                patient.setLastName(rs.getString("last_name"));
                patient.setFirstName(rs.getString("first_name"));
                patient.setGender(rs.getString("gender"));
                patient.setMiddleName(rs.getString("middle_name"));
                patient.setSnils(rs.getString("snils"));

                if(rs.getObject("mo") != null &&
                        rs.getObject("district") != null &&
                        rs.getObject("date_in") != null) {
                    MPIAttachment attachment = new MPIAttachment();
                    attachment.setMo(rs.getString("ogrn"));
                    attachment.setDistrict(rs.getString("district"));
                    attachment.setOwner_snils(rs.getString("owner_snils"));

                    attachment.setDateIn(ldtFormatter.format(rs.getTimestamp("date_in").toLocalDateTime()));

                    patient.setAttachment(attachment);
                }

                patients.add(patient);
            }
        } catch (Exception e) {
            LOG.error(CONSTANTS.MAPPING_PATIENT_ERROR, e);
            return null;
        }
        return patients;
    }
}
