package scheme;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;

public class MPIInsurance {

    @SerializedName("smoCode")
    private String smoCode;

    @SerializedName("smo")
    private String smo;

    @SerializedName("smoName")
    private String smoName;

    @SerializedName("beginDate")
   // @XmlJavaTypeAdapter(XmlLocalDateAdapter.class)
    private LocalDate beginDate;

    @SerializedName("series")
    private String series;

    @SerializedName("number")
    private String number;

    @SerializedName("type")
    private String type;

    public MPIInsurance() {}
    public String getSmoCode() {
        return smoCode;
    }

    public String getSmo() { return smo; }
    public String getSmoName() { return smoName; }
    public LocalDate getBeginDate()
    {
        if(beginDate == null)
            beginDate = LocalDate.now();

        return beginDate;
    }
    public String getSeries() { return series; }
    public String getNumber() { return number; }
    public String getType() { return type; }
}

