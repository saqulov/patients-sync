package scheme;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;

public class MPIAttachment {

    @SerializedName("mo")
    private String mo;

    @SerializedName("moName")
    private String moName;

    @SerializedName("dateIn")
    private String dateIn;

    @SerializedName("district")
    private String district;

    public MPIAttachment(){}
    public void setMo(String mo) {
        this.mo = mo;
    }

    public void setMoName(String moName) {
        this.moName = moName;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setOwner_snils(String owner_snils) {
        this.owner_snils = owner_snils;
    }

    @SerializedName("owner_snils")
    private String owner_snils;

    public String getMo() { return mo; }
    public String getMoName() { return moName; }
    public String getDateIn() { return dateIn; }

    public String getDistrict() {
        return district;
    }

    public String getOwner_snils() {
        return owner_snils;
    }
}
