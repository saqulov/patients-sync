package scheme;

import com.google.gson.annotations.SerializedName;

public class MPIAddress {
    @SerializedName("aoid")
    private String aoid;

    @SerializedName("plain")
    private String plain;

    public MPIAddress(){}
    public String getAoid() { return aoid; }
    public String getPlain() { return plain; }
}
