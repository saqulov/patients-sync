package scheme;

import com.google.gson.annotations.SerializedName;


public class MPIPatient {

    @SerializedName("id")
    private long id;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("middleName")
    private String middleName;
    @SerializedName("snils")
    private String snils;
    @SerializedName("birthDate")
    private String birthDate;
    @SerializedName("gender")
    private String gender;
    @SerializedName("enp")
    private String enp;
    @SerializedName("document")
    private MPIDocument document;
    @SerializedName("insurance")
    private MPIInsurance insurance;

    public MPIPatient(){}

    public void setId(long id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setEnp(String enp) {
        this.enp = enp;
    }

    public void setDocument(MPIDocument document) {
        this.document = document;
    }

    public void setInsurance(MPIInsurance insurance) {
        this.insurance = insurance;
    }

    public void setAttachment(MPIAttachment attachment) {
        this.attachment = attachment;
    }

    public void setAddress(MPIAddress address) {
        this.address = address;
    }

    @SerializedName("attachment")
    private MPIAttachment attachment;
    @SerializedName("address")
    private MPIAddress address;

    public long getId() {
        return id;
    }
    public String getLastName() {
        return lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public String getSnils() {
        return snils;
    }
    public String getBirthDate() {
        return birthDate;
    }
    public String getGender() {
        return gender;
    }
    public String getEnp() { return enp; }
    public MPIDocument getDocument() { return document; }
    public MPIInsurance getInsurance() { return insurance; }
    public MPIAttachment getAttachment() { return attachment; }
    public MPIAddress getAddress() { return address; }
}
