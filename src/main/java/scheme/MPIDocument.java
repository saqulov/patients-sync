package scheme;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;

public class MPIDocument {

    @SerializedName("type")
    private int type;

    @SerializedName("seria")
    private String series;

    @SerializedName("number")
    private String number;

    @SerializedName("issueDate")
    private LocalDate issueDate;

    @SerializedName("issuer")
    private String issuer;

    public MPIDocument(){}
    public int getType() { return type; }
    public String getSeries() { return series; }
    public String getNumber() { return number; }
    public LocalDate getIssueDate() { return issueDate; }
    public String getIssuer() { return issuer; }
}
