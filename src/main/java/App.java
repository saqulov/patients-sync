import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import core.AttachController;
import core.CONSTANTS;
import db.Database;
import db.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scheme.MPIPatient;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class App {
    private final static Logger LOG = LoggerFactory.getLogger(App.class);
    private final static Database database = new Database();

    private  Repository repo;

    public static void main(String[] args) throws IOException {

        new App().transmitAttachments(/*true*/);
    }

    public App() {
        repo = new Repository(database);
    }

    public void transmitUnAttachments() throws IOException {
        Gson gson = new GsonBuilder()
                .setDateFormat(CONSTANTS.DATE_FORMAT)
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        Long startIndex = 0L;

        Long endIndex = repo.getMaxId();

        if(endIndex != -1L)
            while(startIndex <= endIndex) {
                List<MPIPatient> chunk = repo.getChunk(startIndex, 1000);

                try {
                    retrofit2.Response<Void> r = AttachController
                            .getInstance().getApi().syncAttached(chunk).execute();

                    if(!r.isSuccessful())
                        throw new RuntimeException(r.errorBody() == null ? "" : r.errorBody().string());

                } catch (Exception e) {
                    LOG.error(e.getMessage());
                    gson.toJson(chunk,
                            new FileWriter(
                                    "C:" +
                                            File.separator +
                                            "tmp" +
                                            File.separator +
                                            "chunk-start-" +
                                            startIndex +
                                            ".json"));
                    startIndex = getNext(chunk);
                    continue;
                }

                startIndex = getNext(chunk);
            }

        database.terminate();
    }
    public void transmitAttachments() throws IOException {

        Gson gson = new GsonBuilder()
                .setDateFormat(CONSTANTS.DATE_FORMAT)
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        Long startIndex = 0L;

        Long endIndex = repo.getMaxId();

        if(endIndex != -1L)
            while(startIndex <= endIndex) {
            List<MPIPatient> chunk = repo.getChunk(startIndex, 1000);

                try {
                    retrofit2.Response<Void> r = AttachController
                            .getInstance().getApi().syncAttached(chunk).execute();

                    if(!r.isSuccessful())
                        throw new RuntimeException(r.errorBody() == null ? "" : r.errorBody().string());

                } catch (Exception e) {
                    LOG.error(e.getMessage());
                    gson.toJson(chunk,
                            new FileWriter(
                                    "C:" +
                                            File.separator +
                                            "tmp" +
                                            File.separator +
                                            "chunk-start-" +
                                            startIndex +
                                            ".json"));
                    startIndex = getNext(chunk);
                    continue;
                }

                startIndex = getNext(chunk);
        }

        database.terminate();
    }

    private Long getNext(List<MPIPatient> persons) {
        Optional<MPIPatient> opt = persons.stream().max(Comparator.comparing(MPIPatient::getId));

        return opt.map(MPIPatient::getId).orElse(Long.MAX_VALUE);

    }

}
