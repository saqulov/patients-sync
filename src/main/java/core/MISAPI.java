package core;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import scheme.MPIPatient;

import java.util.List;

public interface MISAPI {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("mpi")
    Call<Void> syncAttached(@Body List<MPIPatient> patients);
   // Call<List<MPIPatient>> syncAttached(String persons);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @DELETE("mpi/attachments")
    Call<ResponseBody> syncUnAttached(@Body List<MPIPatient> patients);

}
