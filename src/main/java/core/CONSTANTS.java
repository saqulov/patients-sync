package core;

public class CONSTANTS {
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public static final String BASE_URL = "http://q4-demo2.medsoft.su:8081/mis/api/patient_search/persons/";
    public static final String MAPPING_PATIENT_ERROR = "Ошибка при маппинге пациента";
    public static final String MAPPING_ID_ERROR = "Ошибка при маппинге id";
}
