package core;

import okhttp3.OkHttpClient;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

public class AttachController  {

    private static AttachController instance;

    private MISAPI api;
    private AttachController() {

        Gson gson = new GsonBuilder()
                .setDateFormat(CONSTANTS.DATE_FORMAT)
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(10, TimeUnit.SECONDS);
        httpClient.readTimeout(5, TimeUnit.MINUTES);
        httpClient.writeTimeout(5, TimeUnit.MINUTES);
        httpClient.addInterceptor(logging);
        httpClient.retryOnConnectionFailure(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CONSTANTS.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        api = retrofit.create(MISAPI.class);
    }
    public static AttachController getInstance() {
        if(instance == null)
            instance = new AttachController();

        return instance;
    }

    public MISAPI getApi() {
        return api;
    }

}

